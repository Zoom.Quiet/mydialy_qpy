# -*- coding: utf-8 -*-
import os
import sys
from time import time, gmtime, strftime, localtime





from config import CFG

def INITlogF(fname=CFG.DEFAULT_LOG):
    if os.path.exists(fname):
        return open(fname,"a+")
    else:
        return open(fname,"w+")

def GOTallLog(fname=CFG.DEFAULT_LOG):
    if os.path.exists(fname):
        #print open(fname,'r').read()
        #print type(open(fname,'r').read())
        return open(fname,'r').read()#.decode()
    else:
        return None



if __name__ == '__main__':
    if 2 != len(sys.argv) :
        print '''Usage:
            utility.py test
        '''
    else:
        print "hand testing ..."

