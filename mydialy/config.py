# -*- coding: utf-8 -*-
import sys
from os import uname
import datetime

import os.path
app_root = os.path.dirname(__file__)
sys.path.insert(0, os.path.join(app_root, "3party/"))
sys.path.insert(0, os.path.join(app_root, "module/"))
sys.path.insert(0, os.path.join(app_root, "web/"))
#import hashlib
from xsettings import XCFG

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 全局值
class Borg():
    '''base http://blog.youxu.info/2010/04/29/borg
        - 单例式配置收集类
    '''
    __collective_mind = {}
    def __init__(self):
        self.__dict__ = self.__collective_mind
    
    VERSION = "md7w v15.11.22.2222"
    
    #管理员邮箱列表
    ADMIN_EMAIL_LIST = ['zoomquiet+om@gmail.com']

    if 'ANDROID_PUBLIC' in os.environ:
        # SAE
        AS_SAE = True
    else:
        # Local
        AS_SAE = False

    if AS_SAE:
        _SRV = XCFG.TO_SAE
    else:
        _SRV = XCFG.TO_DEV

    PS0 = "当下>>> "
    TAG0 = "NULL"
    TAGwx= "WCHT"
    TAGqp= "QPY"
    GAP0 = "$#$"
    #   系统索引名-UUID 字典; KVDB 无法Mongo 样搜索,只能人工建立索引
    K4D = {'incr':"SYS_TOT"     # int
        ,'his':"SYS_node_HIS"   # [] 所有 节点的K索引 (包含已经 del/覆盖)
        ,'p':"SYS_notes_ALL"    # [] 所有 笔记
        ,'tag':"SYS_tags_ALL"  # [] 所有 笔记 tag
    }



    
CFG = Borg()
print CFG.VERSION

