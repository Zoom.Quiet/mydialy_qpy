# -*- coding: utf-8 -*-

class Borg():
    '''base http://blog.youxu.info/2010/04/29/borg
        - 单例式配置收集类
    '''
    __collective_mind = {}
    def __init__(self):
        self.__dict__ = self.__collective_mind

    TO_SAE = "http://zoomquiet.sinaapp.com/cli/log"
    TO_DEV = "http://127.0.0.1:8080/cli/log"
    DEV2wx = "http://127.0.0.1:8080/api/wx"


XCFG = Borg()


