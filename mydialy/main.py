#-*-coding:utf8;-*-
#qpy:2
#qpy:webapp:mydialy 
#qpy://localhost:18090/

"""
This is a sample for qpython webapp
"""
from bottle import run,debug
import sys
import os.path
app_root = os.path.dirname(__file__)
sys.path.insert(0, app_root)
from web import APP

#application = sae.create_wsgi_app(APP)

debug(True)
#0.0.0.0
run(APP
    , host="127.0.0.1"
    , port=18090, reloader=True)
